module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
    content: [
        './header.php',
        './footer.php',
        './single.php',
	'./sidebar.php',
	'./functions.php',
	'./inc/template-tags.php',
	'./inc/template-functions.php',
	'./template-parts/content.php',
	'./template-parts/comments.php',
	'./template-parts/content-page.php',
    ],
  theme: {
    extend: {},
  },
  plugins: [],
}
