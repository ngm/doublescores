<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package doublescores
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

  <link href="https://fonts.googleapis.com/css?family=Nunito:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
  <style>
   body {
       font-family: 'Nunito', sans-serif;
   }
  </style>
</head>

<body>
<div id="page" class="container ml-auto sm:ml-10 mr-auto">
	  <!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'doublescores' ); ?></a> -->

	<header id="masthead" class="site-header py-4">
		<div class="flex flex-col sm:flex-row sm:items-center sm:justify-between">
        <span class="flex flex-row sm:flex-row items-center sm:justify-between">
            <?php the_custom_logo(); ?>
            <!-- <h1 class="text-lg sm:text-2xl sm:pl-2 ml-5"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> -->

            <nav id="site-navigation" class="main-navigation sm:pl-2 ml-5">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                    'menu_id'        => 'primary-menu',
                ) );
                ?>
            </nav><!-- #site-navigation -->
        </span>

        <?php
			  $doublescores_description = get_bloginfo( 'description', 'display' );
			  if ( $doublescores_description || is_customize_preview() ) :
				?>
				    <p class="text-lg w-full hidden sm:block sm:w-1/2 sm:text-right ml-5 sm:ml-0 mr-1"><?php echo $doublescores_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

	</header><!-- #masthead -->

	<div id="content" class="flex flex-col sm:flex-row">
