<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package doublescores
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry ds-note'); ?>>
<div>
	<header class="entry-header">
		  <?php the_title( '<h1 class="leading-none text-lg text-gray-700 p-name font-sans font-bold mb-2">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php doublescores_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'doublescores' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'doublescores' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</div>
</article><!-- #post-<?php the_ID(); ?> -->
