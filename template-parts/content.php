<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package doublescores
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry ds-note'); ?>>
	<?php doublescores_post_thumbnail(); ?>

	<div class="sm:w-8/12 mr-auto max-w-full" >
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'doublescores' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'doublescores' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

  <div class="sm:w-3/12 text-sm px-2 ml-3 text-right border-t sm:border-t-0 sm:border-l flex flex-col mt-4 sm:mt-0 justify-between">
    <header class="my-auto flex-1">
<?php
if ( 'post' === get_post_type() ) :
?>
    <div class="entry-meta">
        <?php
        doublescores_posted_on();
        doublescores_posted_by();
        ?>
    </div><!-- .entry-meta -->
<?php endif; ?>
<?php if ( is_singular() ) :
                        the_title( '<h1 class="leading-none text-lg text-gray-700 p-name font-sans font-bold mb-2">', '</h1>' );
                    else :
                        the_title( '<h2 class="leading-none text-lg text-gray-700 p-name font-sans font-bold mb-2"><a class="no-underline" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
endif;
?>

</header><!-- .entry-header -->

<footer class="entry-footer">
<?php doublescores_entry_footer(); ?>
</footer><!-- .entry-footer -->
</div>


</article><!-- #post-<?php the_ID(); ?> -->
